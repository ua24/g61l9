//
//  ViewController.swift
//  G61L9
//
//  Created by Ivan Vasilevich on 3/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
	
	@IBOutlet weak var tableView: UITableView!
	
	var objects = [String]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		setupData()
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let indexPath = sender as? IndexPath {
			let objectToTransmit = objects[indexPath.row]
			if let cashTVC = segue.destination as? CashTVC {
				cashTVC.stringFromPrevVC = objectToTransmit
			}
		}
	}
	
	func setupData() {
		let text = """
//  Created by Иван on 07.03.18.
//  Copyright © 2018 Ivan. All rights reserved.
"""
		let words = text.components(separatedBy: " ")
		objects = words
	}
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "wordCell", for: indexPath)
		cell.textLabel?.text = objects[indexPath.row]
		return cell
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath.row)
		performSegue(withIdentifier: "showCash", sender: indexPath)
	}
}

